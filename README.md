# Search My Site blog

This repo contains the source files for the website at [https://blog.searchmysite.net/](https://blog.searchmysite.net/).

This document shows the steps to create this website.


# How this site was created

These steps were a one-off operation, and are here just for reference. The steps assume Hugo is installed and git configured.


## Create Hugo site and add to GitLab


Create new project in GitLab at https://gitlab.com/michael-lewis/blog.searchmysite.net.

Clone locally with:
```
cd ~/projects
git clone git@gitlab.com:michael-lewis/blog.searchmysite.net.git
```

Create a new hugo project at that location (`--force` is required because directory exists):
```
hugo new site --force blog.searchmysite.net
```

Check it is all looking okay in git:
```
cd blog.searchmysite.net/
git status
```
You'll just see README.md, archetypes/default.md and config.toml as changes to be committed because content/, data/, layouts/, static/ and themes/ are currently empty.

Add a theme as a submodule to test, e.g.:
```
git submodule add https://github.com/alanorth/hugo-theme-bootstrap4-blog.git themes/hugo-theme-bootstrap4-blog
```

Move the existing config.toml and get the sample one, e.g.:
```
mv config.toml config.toml.bak
wget https://raw.githubusercontent.com/alanorth/hugo-theme-bootstrap4-blog/master/exampleSite/config.toml
```

Edit the config.toml accordingly, and create a sample post in content/posts (posts will need to be created). Start the hugo web server to preview on [http://localhost:1313/](http://localhost:1313/):
```
hugo server -D
```

If all looks good, restore the original config.toml:
```
rm config.toml
cp config.toml.bak config.toml
```

After testing, remove theme via:
```
git submodule deinit -f themes/hugo-theme-bootstrap4-blog/
rm -rf .git/modules/themes/hugo-theme-bootstrap4-blog/
git rm -f themes/hugo-theme-bootstrap4-blog
```

And develop custom theme in themes/searchmysite.


## Configure continuous integration

As per [https://gohugo.io/hosting-and-deployment/hosting-on-gitlab/](https://gohugo.io/hosting-and-deployment/hosting-on-gitlab/), create a .gitlab-ci.yml in the root of the project containing the following:
```
image: monachus/hugo

variables:
  GIT_SUBMODULE_STRATEGY: recursive

pages:
  script:
  - hugo
  artifacts:
    paths:
    - public
  only:
  - master
```

It would also be worth having a .gitignore file in the root of the project with the following:

```
/public
/resources
/server
```

Add, commit and push all of these changes to git:
```
git add .
git commit -m "Initial commit"
git push -u origin master
```

Check pipeline, and once complete site should be at [https://michael-lewis.gitlab.io/blog.searchmysite.net/](https://michael-lewis.gitlab.io/blog.searchmysite.net/). Styles might not load correctly at that location depending on the value of baseurl in config.toml.


# Setting up DNS

As per [https://about.gitlab.com/blog/2016/04/07/gitlab-pages-setup/#custom-domains](https://about.gitlab.com/blog/2016/04/07/gitlab-pages-setup/#custom-domains) go to Settings / Pages / New Domain, add blog.searchmysite.net. Keep the default force HTTPS and use LetsEncrypt certificates.

Add an A record for blog.searchmysite.net to 35.185.44.232, add the validation code TXT record.


