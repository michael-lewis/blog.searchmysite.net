---
title: "Switching to cheaper hosting and stopping indexing wikipedia to cut costs"
date: "2022-03-05"
lastmod: "2022-03-05"
tags: ["search", "searchmysite.net", "wikipedia", "hosting"]
keywords: ["search", "searchmysite.net", "wikipedia", "hosting"]
description: "A short post on the new hosting provider and cost savings."
categories: ["searchmysite.net", "personal website"]
draft: false
---



## Priorities for 2022

As detailed in the [searchmysite.net retrospective and future plans](/posts/searchmysite.net-retrospective-and-future-plans/), one of the priorities for 2022 was moving to a cheaper hosting provider (another was to spend less time writing blog entries, hence this being a brief post).

## Cheaper hosting

I had hoped to use [fosshost.org](https://fosshost.org), but unfortunately they're not taking new projects. Of the paid alternatives, I chose [hetzner.com](https://www.hetzner.com/) which appeared around a quarter of the cost of the previous hosting and got reasonable mentions in places like [Hacker News](https://hn.algolia.com/?q=hetzner).

## Stopping indexing wikipedia

Given I need a minimum of 4Gb RAM (ideally 8Gb) and 250Gb disk if continuing to [index wikipedia](/posts/searchmysite.net-now-with-added-wikipedia-goodness/) the options were still pretty expensive, e.g. EUR27.25 monthly for the CPX41. However, if I stopped indexing wikipedia, I could get away with 80Gb disk, in which case the CPX21 for EUR8.28 monthly would be fine. So I have removed the wikipedia content. It can be readded later if there is renewed interest, matched by sufficient [paid search as a service subscriptions](/posts/searchmysite.net-the-delicate-matter-of-the-bill/).

