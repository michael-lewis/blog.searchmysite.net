---
title: "Milestone: 2,000th site indexed"
date: "2023-12-16"
lastmod: "2023-12-16"
tags: ["search", "searchmysite.net"]
keywords: ["search", "searchmysite.net"]
description: "The 2,000th site has been indexed by searchmysite.net."
categories: ["searchmysite.net", "personal website"]
draft: false
---

## Milestones

- 17 Jul 2020 - [the first site submitted by a real user and indexed](https://blog.searchmysite.net/posts/searchmysite.net-building-a-simple-search-for-non-commercial-websites/).
- 12 Mar 2022 - [the 1000th site](https://blog.searchmysite.net/posts/milestone-1000th-site-indexed/), around 20 months later.
- 15 Dec 2023 - the 2000th site, roughly 21 months after the 1000th.

## The 2,000th site indexed by searchmysite.net is...

[paulrobichaux.com](https://paulrobichaux.com/). Many thanks for submitting!

## How many sites could searchmysite.net contain?

I ran a poll on Mastodon asking [How many people in the world actively maintain a personal website?](https://fosstodon.org/@michaellewis/111583808331037861) The most popular answer was 80,000, i.e. roughly 1 out of every 100,000 people in the world.

If that is correct, searchmysite.net only has around 2.5% of actively maintained personal websites, which means there's still a long way to go!

So please keep the site submissions coming. Focusing on getting more sites is certainly the easiest and probably the best way of making searchmysite.net better and achieving its goal of helping people find some of the great human written content which is difficult to find on the major search engines nowadays.

