---
title: "searchmysite.net now has its own blog"
date: "2020-11-21"
lastmod: "2020-11-21"
tags: ["search", "search as a service", "searchmysite.net", "blog", "setting up a blog", "hugo and gitlab pages", "creating a hugo theme"]
keywords: ["search", "search as a service", "searchmysite.net", "blog", "setting up a blog", "hugo and gitlab pages", "creating a hugo theme"]
description: "Welcome to blog.searchmysite.net. I've split it from my personal website because searchmysite.net does seem to be starting to get a life of its own now."
draft: false
---


## Welcome

Welcome to the first post on searchmysite.net's new blog. Well the first one first posted on this blog - there are actually some earlier posts which I've copied over from my [personal site](https://michael-lewis.com/) where they were first published:

- [searchmysite.net: Building a simple search for non-commercial websites](/posts/searchmysite.net-building-a-simple-search-for-non-commercial-websites/) (18 Jul 2020)
- [searchmysite.net update: Seeding and scaling](/posts/searchmysite.net-update-seeding-and-scaling/) (25 Sep 2020)
- [Adding a simple search page to my personal website with searchmysite.net](/posts/adding-a-simple-search-page-to-my-personal-website-with-searchmysite.net/) (9 Oct 2020)

I decided to split the blogs out because there's quite a bit I want to write on both, and searchmysite.net does seem to be starting to get a life of its own now.


## Building this blog with Hugo and GitLab pages

I decided to use Hugo and GitLab pages because that's what I used for my personal site (see [building a website with Hugo and GitLab Pages](https://michael-lewis.com/posts/building-a-site-with-hugo-and-gitlab/)) and wanted to get it up and running in the shortest time, plus it works pretty well. I've jotted down the main steps below for reference.


### Creating the project in GitLab

First step was to create a new project in GitLab - in this case at [https://gitlab.com/michael-lewis/blog.searchmysite.net](https://gitlab.com/michael-lewis/blog.searchmysite.net).

Then clone locally with:
```
cd ~/projects
git clone git@gitlab.com:michael-lewis/blog.searchmysite.net.git
```

Create a new hugo project at that location (`--force` is required because directory exists):
```
hugo new site --force blog.searchmysite.net
```

Add a theme (more on that below), edit the config.toml accordingly, create a sample post in content/posts (the posts directory will need to be created), and start the hugo web server to preview on [http://localhost:1313/](http://localhost:1313/):
```
hugo server -D
```


### Configuring continuous integration in GitLab

As per [https://gohugo.io/hosting-and-deployment/hosting-on-gitlab/](https://gohugo.io/hosting-and-deployment/hosting-on-gitlab/), create a .gitlab-ci.yml in the root of the project containing the following:
```
image: monachus/hugo

variables:
  GIT_SUBMODULE_STRATEGY: recursive

pages:
  script:
  - hugo
  artifacts:
    paths:
    - public
  only:
  - master
```

Although not strictly necessary, I also created a .gitignore file in the root of the project with the following:

```
/public
/resources
/server
```

Final step was to add, commit and push all of these changes to git:
```
git add .
git commit -m "Initial commit"
git push -u origin master
```

Once the pipeline had completed, the new site was visible at [https://michael-lewis.gitlab.io/blog.searchmysite.net/](https://michael-lewis.gitlab.io/blog.searchmysite.net/).


### Setting up DNS

As per [https://about.gitlab.com/blog/2016/04/07/gitlab-pages-setup/#custom-domains](https://about.gitlab.com/blog/2016/04/07/gitlab-pages-setup/#custom-domains) I went to Settings / Pages / New Domain, and added blog.searchmysite.net. I kept the default force HTTPS and use LetsEncrypt certificates.

Finally, in my domain provider's interface, I added an A record for blog.searchmysite.net to 35.185.44.232 (the GitLab Pages IP address), and added the validation code TXT record so GitLab knew I was the owner of the domain and could get the SSL cert on my behalf.


### Creating a new Hugo theme

I did try a number of different [Hugo themes](https://themes.gohugo.io/). It was relatively straight forward to switch between them to try them out. Unfortunately none of the ones I tried quite fitted in with the ultra basic design of searchmysite.net, so ended up writing my own theme. I started out referring to the nicely detailed [Let's Create a New Hugo Theme](https://www.pakstech.com/blog/create-hugo-theme/), although then switched to the [Hugo templates](https://gohugo.io/templates/) documentation when I started getting the hang of it.


## Conclusion

Setup was quite straightforward, although helped by following notes I'd taken previously. I hadn't developed a Hugo theme before though, and was quite pleasantly surprised at how much work Hugo did for you (or how little you actually had to do). I know what you see here is about as basic as it gets, but it is (currently) less than 150 lines of HTML in just 7 files, which is nice.

Anyway, now I've a dedicated blog for searchmysite.net, there are quite a few updates on progress that I'd like to make. Watch out for these in coming weeks. There's an [RSS feed](https://blog.searchmysite.net/index.xml) if you like.

