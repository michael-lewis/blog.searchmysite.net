---
title: "Milestone: 1,000th site indexed"
date: "2022-03-12"
lastmod: "2022-03-12"
tags: ["search", "searchmysite.net"]
keywords: ["search", "searchmysite.net"]
description: "The 1,000th site indexed by searchmysite.net is weeklymusings.net."
categories: ["searchmysite.net", "personal website"]
draft: false
---

## The 1,000th site indexed by searchmysite.net is...

[weeklymusings.net](https://weeklymusings.net/) run by [scottnesbitt.online](https://scottnesbitt.online/):

![1000th site](/images/posts/1000thsite-small.gif)

It was submitted and indexed on Monday 7 March 2022. (The 1,001st site indexed was [uh.edu/engines](https://uh.edu/engines/).)

It was part of a flurry of new submissions, thanks largely to the mention at [hubme.it/search-my-site](https://hubme.it/search-my-site).

Note that it is the 1,000th site indexed rather than submitted. Many more sites have been submitted than are currently indexed, because some (currently 357) have been submitted but not approved for indexing, and some (currently 43, i.e. around 4-5%) have been approved but have subsequently had indexing disabled due to the indexing errors detailed in [Some of the challenges of building an internet search](/posts/some-of-the-challenges-of-building-an-internet-search/).
