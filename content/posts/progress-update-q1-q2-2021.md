---
title: "Progress update Q1 & Q2 2021"
date: "2021-06-11"
lastmod: "2021-06-11"
tags: ["search", "searchmysite.net", "bugs", "analytics"]
keywords: ["search", "searchmysite.net", "bugs", "analytics"]
description: "This is just a quick update on progress since the last post on 30 Jan 2021."
categories: ["searchmysite.net"]
draft: false
---

## Introduction

This is just a quick update on progress. Since the last post on 30 Jan 2021 I have:
- Been checking the new submissions on a daily basis and approving/rejecting accordingly.
- Been checking and responding to emails.
- Made a couple of minor maintenance releases, primarily bug fixes - details at [https://github.com/searchmysite/searchmysite.net/releases](https://github.com/searchmysite/searchmysite.net/releases).


## Stability

I'm also pleased to report that there have been no outages in the first 6 months of this year. There have been two occasions where the indexing got stuck, i.e. new submissions could not be indexed and existing sites could not be reindexed, but users could search previously indexed sites so it was not really a visible outage as such:
- One was simply because the server ran out of disk space. Details are at [https://github.com/searchmysite/searchmysite.net/issues/31](https://github.com/searchmysite/searchmysite.net/issues/31), but in summary I increased disk space and set up some alerts. A couple of weeks back I got an alert that the server was nearly out of disk space again, and I was able to increase disk space prior to it causing any issues, which was good.
- The other was a caused by a problematic custom indexing filter which a user had entered. Details are at [https://github.com/searchmysite/searchmysite.net/issues/32](https://github.com/searchmysite/searchmysite.net/issues/32), but in summary a fix was implemented, although it looks like the custom indexing filter functionality may still lead to other issues in future.


## Usage levels

Here's a snapshot of site usage since I implemented [Web analytics on searchmysite.net](/posts/web-analytics-on-searchmysite.net/):

![searchmysite.net analytics Nov 2020 - Jun 2021](/images/posts/analytics202011-202106.gif)

It would have been nice to report steadily increasing usage, but at least usage levels have not been declining significantly.

One thing that is quite interesting is that Google does not appear in the top referrers. In fact, for the blog at blog.searchmysite.net, I have never had a single referrer from Google, even though all the blog posts have been in the Google search index for many months. This is especially odd given that, for my personal blog, I get twice as much traffic from Google as from every other source added together.

I actually spent some time at the start of the year trying to increase the traffic from Google for both searchmysite.net and blog.searchmysite.net. Key activities included:
- Keyword analysis, and modifying keywords, titles and descriptions accordingly.
- Putting one h1 tag on all pages, and making other headings more descriptive.
- Ensuring titles and descriptions were of the optimum length (between 25 and 160 characters).
- Making improvements to performance, given performance is supposed to factor into the score. These increased my [Lighthouse score](https://developers.google.com/web/tools/lighthouse) from Performance 90, Accessibility 97, Best Practices 100, SEO 100, to Performance 98, Accessibility 97, Best Practices 100, SEO 100.

I had hoped to write a "How I increased my site traffic x times with Search Engine Optimisation" blog post after these changes, which would have been slightly ironic given [this site's stance on SEO](/posts/advertising-and-search-engines-when-it-is-okay-to-mix-and-when-it-is-not/) (or more specifically black-hat SEO, aka spamdexing), but given the traffic did not increase at all it was not to be.

