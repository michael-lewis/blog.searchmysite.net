---
title: "Advertising and search engines: When it is okay to mix and when it is not"
date: "2020-12-17"
lastmod: "2021-01-17"
tags: ["search", "searchmysite.net", "adverts", "SEO", "SEO and advertising"]
keywords: ["search", "searchmysite.net", "adverts", "SEO", "SEO and advertising"]
description: "searchmysite's approach to advertising has attracted a lot of positivity, but some negativity, so this post is to clarify the position on advertising and search engines."
categories: ["searchmysite.net"]
draft: false
---

searchmysite.net is an open source search engine and search as a service for personal and independent websites, which has a unique approach to advertising to try to tackle spam. I mentioned this approach to advertising in my [last post](/posts/searchmysite.net-is-now-open-source/). While this attracted a lot of positivity, it also unfortunately got some negativity[^note1], so I thought I'd write a quick post to clarify my position on advertising and search engines, hopefully in positive terms.

In summary, searchmysite.net downranks pages containing adverts, to lower the influence of results which might be trying to game the system via Search Engine Optimisation (SEO), clickbait content, and so on. It doesn't completely block pages with adverts, just downranks them (as the most recent [relevancy tuning post](/posts/relevancy-tuning-for-searchmysite.net/) explains, if a page is important enough to have a lot of inlinks then those can compensate for the presence of adverts). I wasn't suggesting search engines which promote pages with adverts should be banned, or even that I think they would wither away if there was an alternative. I do think there is a time and place for advertising. Like when you want to buy something. But sometimes you just want to read something.

Using a pre-internet analogy, imagine you live in a town which has a high street filled with shops trying to sell you things, and a public library filled with books trying to tell you things. If you want to buy something you go down the high street to look at the shops, and if you want to read something you go into the library to look for a book. Both activities can co-exist - good libraries do not put shops out of business or vice versa. But you don't want both activities to happen at the same time - you don't want a library filled with salespeople trying to get between you and your books, because that would be distinctly unhelpful at best and downright annoying at worst.

The problem is that the internet has turned into a city that is all shops and no libraries. Not even local independent shops at that, just giant out-of-town retail malls (but that's another story). I know there's Wikipedia, which is wonderful, but for the purposes of this analogy Wikipedia is more like a multi-volume encyclopedia inside a library rather than the library itself. The other interesting books still exist, but without a library they're hard to find, scattered around the streets and alleys, maybe in a public bookcase if you are lucky, with only the most commercially viable ones available in shops.

The end result is that every single search engine, from the biggest tech giants to the smallest new startups, are chasing their cut of the multi-billion dollar advertising market and/or buying in SEO-affected results from one of the big existing search engines. That's why I believe there is a space for a search engine that is different, like searchmysite.net. Not everything on the internet is a marketing funnel, and there are times when you don't want the effects of advertising in the way. This should not be seen as a threat to the established order, rather a complement. Although if certain people are seeing it as a threat then that is actually oddly reassuring.


[^note1]: Presumably the negativity came from people trying to make money from advertising, e.g. SEO practitioners, people who work in the AdTech industry, or people trying to make money by blogging about how to make money by blogging.

