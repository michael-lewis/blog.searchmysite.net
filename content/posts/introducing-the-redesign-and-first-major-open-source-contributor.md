---
title: "Introducing the redesign and first major open source contributor"
date: "2023-08-19"
lastmod: "2023-08-22"
tags: ["search", "searchmysite.net", "redesign", "open source"]
keywords: ["search", "searchmysite.net", "redesign", "open source"]
description: "The searchmysite.net redesign has been launched, thanks to its first major open source contributor Lucas Gramajo."
categories: ["searchmysite.net"]
draft: false
---

Some exciting news, first hinted at in the [Three year retrospective](/posts/three-year-retrospective/): the searchmysite.net redesign has been launched, thanks to its first major open source contributor [Lucas Gramajo](https://design.retromodern.ca/).

## First major open source contributor

Just to clarify, there have been other open source contributors to searchmysite.net before. The first was [Binyamin](https://binyam.in/), who contributed the favicon on 23 Oct 2020. And after searchmysite.net was [fully open sourced](/posts/searchmysite.net-is-now-open-source/) in Dec 2020, there have been other contributions in the form of bug reports, discussion etc. Plus of course searchmysite.net is entirely based on other open source projects like [Apache Solr](https://solr.apache.org/), so without all the contributions to those great projects, this project wouldn't exist.

But this is the first *major* open source contributor to this specific project, contributing lots of code over the space of several months for the [logo and redesign](https://github.com/searchmysite/searchmysite.net/issues/28). Many thanks, and welcome aboard Lucas!

Hopefully this will encourage other contributions too. Now a lot of the "pipes and plumbing" (like password reset pages) is largely complete, some of the future enhancements should be more fun and interesting, e.g. the [vector search](https://github.com/searchmysite/searchmysite.net/issues/99) recently rolled out, and the [Large Language Model (LLM)](https://github.com/searchmysite/searchmysite.net/issues/96) work in the pipeline.


## Redesign

One of the main objectives of searchmysite.net is to help people discover original, creative, interesting and human-made content. Back in the early days, the whole web was like that. Some people think that that web has completely disappeared, but it hasn't - it is still there, but it is just difficult to find, hidden under all the mountains of garbage that the big search engines dump at the top of the results for everyone to maximise their advertising profits.

Given this objective, it made sense for the redesign to hark back somewhat to the early days of the web. Not with blink tags and "under construction" animations, but with something more sophisticated and evolved. Hence the "retro modernism" aesthetic.

Lucas approached the project in a thoroughly professional way, providing a mood board to determine the spirit of the project, along with multiple logo options and colour schemes, and plenty of discussion along the way, before beginning any template development.

From the mood board a number of design principles were derived, e.g. "slick should trump retro", "open source values [but] pragmatic and approachable before hardcore and dogmatic", "community, professional, trust, approachable, friendly, welcoming".

The logo options included a "friendly with mascot concept", with a couple of dog options (fetching) and a rabbit option (speed). I still like the concept of a friendly and helpful animal, but in the end the abstract logo, to represent "data streams", won out. Note that it is a hand-crafted logo, to fit more with the "real content by real people" ethos.

I'll conclude with Lucas's own words: "This design style I would call 'A List Apart', in homage of this site: https://web.archive.org/web/20060224101301/https://alistapart.com/ ... it was a thing, especially when Movable Type and WordPress started, they were making everything look very old school press like. ... The printing press, going back to personal blogs (with no ads) from the beginning of the millennium, nostalgia, etc..." and my response: "I do like that almost-steampunk old-but-new type of design. ... a vintage that has stood the test of time so well that it doesn't look out of place nowadays. ... I think the letterpress model works well. ... blogs / personal websites are the internet equivalent of home printing. ... I'd love to think the old web could come back to dominate again, but I don't think that's realistic. The best I can hope for is for an alternative non-commercial web to thrive in parallel to the commercial web, like a licit version of the dark web (the light web?)"

