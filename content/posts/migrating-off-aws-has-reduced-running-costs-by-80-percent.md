---
title: "Migrating off AWS has reduced running costs by 80%"
date: "2022-12-10"
lastmod: "2022-12-26"
tags: ["search", "searchmysite.net", "hosting", "aws", "hetzner"]
keywords: ["search", "searchmysite.net", "hosting", "aws", "hetzner"]
description: "This is a short post comparing previous running costs on AWS and current running costs on Hetzner."
categories: ["searchmysite.net"]
draft: false
---


## Running costs

This is a short post comparing previous running costs on AWS and current running costs on Hetzner. Previous posts with details of running costs are [searchmysite.net: The delicate matter of the bill](/posts/searchmysite.net-the-delicate-matter-of-the-bill/) from Jan 2021, and the [Escalating running costs section in the searchmysite.net retrospective and future plans](/posts/searchmysite.net-retrospective-and-future-plans/#escalating-running-costs-now-nearly-1000-per-year) from Jan 2022.

### AWS

| Month          |&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Cost |&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GBP |
| -------------- | ------: | -------: |
| July 2020      |   $9.04 |    £7.30 |
| August 2020    |  $20.47 |   £15.56 |
| September 2020 |  $28.95 |   £21.49 |
| October 2020   |  $41.09 |   £31.72 |
| November 2020  |  $44.64 |   £34.42 |
| December 2020  |  $47.13 |   £35.36 |
| January 2021   |  $47.35 |   £34.69 |
| February 2021  |  $44.43 |   £32.43 |
| March 2021     |  $47.94 |   £34.43 |
| April 2021     |  $47.70 |   £34.60 |
| May 2021       |  $49.11 |   £35.31 |
| June 2021      |  $48.54 |   £34.26 |
| July 2021      |  $49.54 |   £35.86 |
| August 2021    |  $53.78 |   £38.51 |
| September 2021 |  $53.22 |   £38.67 |
| October 2021   |  $65.47 |   £48.34 |
| November 2021  |  $75.85 |   £55.41 |
| December 2021  |  $77.00 |   £57.85 |
| January 2022   |  $77.00 |   £57.13 |
| February 2022  |  $78.27 |   £58.04 |
| March 2022     |  $52.01 |   £38.81 |
| April 2022     |   $3.46 |    £2.63 |
| May 2022       |   $0.00 |    £0.00 |

### Hetzner

| Month          |&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Cost   |&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GBP |
| -------------- | ------: | -------: |
| March 2022     |   €6.77 |    £5.64 |
| April 2022     |   €6.97 |    £5.86 |
| May 2022       |   €6.97 |    £5.85 |
| June 2022      |   €8.36 |    £7.44 |
| July 2022      |   €8.36 |    £7.21 |
| August 2022    |   €8.36 |    £7.46 |
| September 2022 |   €8.36 |    £7.53 |
| October 2022   |   €8.36 |    £7.53 |
| November 2022  |   €8.36 |    £7.44 |

### Notes

- Servers are of similar specifications: AWS was a t3.medium EC2 instance with 2 vCPUs and 4Gb RAM, while Hetzner is a CPX21 instance also with 2 vCPUs and 4Gb RAM.
- There have not been stability or performance issues with either provider. An instance with 2 vCPUs and 4Gb RAM has been fine for indexing nearly 6,500,000 pages, and handling up to 166K searches a day from spam bots and up to 1.7K searches a day from real users[^note1].
- There was a big monthly increase in running costs starting in October 2021 when I [began indexing Wikipedia](/posts/searchmysite.net-now-with-added-wikipedia-goodness/), and ending in March 2022 when I [stopped indexing Wikipedia](/posts/switching-to-cheaper-hosting-and-stopping-indexing-wikipedia-to-cut-costs/), largely due to additional disk space requirements.
- Excluding the period when Wikipedia was indexed (for a fair comparison), AWS was around £35 a month while Hetzner is around £7 a month, so Hetzner is around 80% cheaper for an equivalent service.
- In addition, AWS costs were unpredictable and increased significantly over time as usage increased, while Hetzner costs have been much more stable. This makes it easier to budget, and perhaps more importantly makes it feel less like I am paying to service the spam bots. 
- There were two months (March and April 2022) when I was paying for two providers, because (i) I simply switched off the AWS servers, in case the migration failed and I needed to switch back, but was charged almost the full amount for the switched off servers, and (ii) when I decided the migration was complete and irreversible, I thought I had deleted absolutely everything on AWS, but kept on finding I'd missed things like backups and the Elastic IP address, so it took nearly 2 months to get the AWS bills to zero.


## Income

### Paid listings

| Month           | Income |
| --------------- | -----: |
| February 2021   | £11.45 |
| March 2021      | £11.45 |
| April 2021      | £22.90 |
| May 2021        |  £0.00 |
| June 2021       |  £0.00 |
| July 2021       |  £0.00 |
| August 2021     |  £0.00 |
| September 2021  |  £0.00 |
| October 2021    |  £0.00 |
| November 2021   | £11.45 |
| December 2021   | £11.45 |
| January 2022    | £11.63 |
| February 2022   | £23.26 |
| March 2022      |  £0.00 |
| April 2022      | £22.90 |
| May 2022        | £11.45 |
| June 2022       |  £0.00 |
| July 2022       | £11.45 |
| August 2022     |  £0.00 |
| September 2022  | £11.50 |
| October 2022    |  £0.00 |
| November 2022   |  £0.00 |

### Notes

- There have now been 4 months (April, May, July and September 2022) where the income has exceeded running costs, so the project has nearly become self-sustaining.



[^note1]: The peak of [spam bot](/posts/almost-all-searches-on-my-independent-search-engine-are-now-from-seo-spam-bots/) searches (166,742 searches in one day) was on Tue 17 May 2022, but has fallen to the low 1000s per day now. The peak of real user searches (1,722 searches in one day) was on Mon 16 May 2022 when searchmysite.net was featured on [Hacker News](https://news.ycombinator.com/item?id=31395231), but has fallen back to single digits per day now. That of course does mean usage is still around 99.9% spam bots.

